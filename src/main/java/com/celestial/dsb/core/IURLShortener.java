/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

/**
 *
 * @author Selvyn
 */
public interface IURLShortener
{
    public String shortenURL(String longURL);

    // expandURL
    // public method which returns back the original URL given the shortened url
    public String expandURL(String shortURL);

    // Validate URL
    // not implemented, but should be implemented to check whether the given URL
    // is valid or not
    public  boolean validateURL(String url);

    // sanitizeURL
    // This method should take care various issues with a valid url
    // e.g. www.google.com,www.google.com/, http://www.google.com,
    // http://www.google.com/
    // all the above URL should point to same shortened URL
    // There could be several other cases like these.
    public  String sanitizeURL(String url);
}
